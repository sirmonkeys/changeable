<?php

/**
 * This file is part of the propel-changeable-behavior package.
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license TBA
 */

namespace ChrisChase\Behaviors\Changeable;

use Propel\Generator\Model\Behavior;

/**
 * Sets up has table changed tracking etc
 *
 * @author Christopher Chase
 */
class ChangeableBehavior extends Behavior {
	protected $parameters = array('changed_column' => "Changed");
	 protected $dirname =  __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;

	public function modifyTable():void {
		$table = $this -> getTable();
		$columnName = $this -> getParameter('changed_column');
		// add the column if not present
		if (!$this -> getTable() -> hasColumn($columnName)) {
			$column = $this -> getTable() -> addColumn(array('name' => $columnName, 'type' => 'TIMESTAMP', ));
		}
	}

	public function objectMethods() {
		$script = "";
		$script .= $this -> addControls();
		return $script;
	}

	public function preSave() {
		$table = $this -> getTable() -> getName();
		$columnName = $this -> getParameter('changed_column');

		return <<<PRESAVE
if (!\$this->isColumnModified('$table.$columnName')) {
		\$this->markChanged();
}
PRESAVE;

	}

	public function queryMethods() {
		$table = $this -> getTable() -> getName();
		$columnName = $this -> getParameter('changed_column');

		return $this -> renderTemplate('Query', array('table' => $table, 'columnName' => $columnName));
	}

	protected function addControls() {
		$table = $this -> getTable() -> getName();
		$columnName = $this -> getParameter('changed_column');

		return $this -> renderTemplate('Controls', array('table' => $table, 'columnName' => $columnName));

	}

}
