/**
 * Set the value of [<?php echo $columnName; ?>] column to 'Now'
 * Marking this <?php echo $table; ?> as being changed 
 *  
 * @return $this, The current object (for fluent API support)
 */
public function markChanged()
{
	return $this->set<?php echo $columnName; ?>(time());
}

/**
 * Checks the value of the [<?php echo $columnName; ?>] column
 * If its set, returns true. I.E. This <?php echo $table; ?> has been changed
 * 
 * @return boolean
 */
public function hasChanged()
{
	if ($fld = $this->get<?php echo $columnName; ?>()){
		return !empty($fld);
	}
	return false;  
}

/**
 * Gets the time that the the [<?php echo $table; ?>] was changed 
 *
 * @param  string $format The date/time format string (either date()-style or strftime()-style).
 *                            If format is NULL, then the raw DateTime object will be returned.
 *
 * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
 *
 * @throws PropelException - if unable to parse/validate the date/time value.
 */
public function getChangedAt($format = NULL)
{
	if ($fld = $this->get<?php echo $columnName; ?>()){
		return $fld;
	}
	return null;
}


/**
 * Set the value of [<?php echo $columnName; ?>] column to NULL
 * Clearing this <?php echo $table; ?>, I.E. Not changed
 * 
 * @return $this, The current object (for fluent API support)
 */
public function unmarkChanged()
{
	return $this->set<?php echo $columnName; ?>(NULL);
}