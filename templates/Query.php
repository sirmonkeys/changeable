/**
 * Filter the query by if the <?php echo $table; ?>s have changed 
 *
 * @param     mixed $key Primary key to use for the query
 *
 * @return $this, The current query, for fluid interface
 */
public function filterByIsChanged()
 {
      return $this->filterBy<?php echo $columnName; ?>(null, CRITERIA::ISNOTNULL);
 }
	
/**
 * Filter the query by if the <?php echo $table; ?>s have not changed 
 *
 * @param     mixed $key Primary key to use for the query
 *
 * @return $this, The current query, for fluid interface
 */	
public function filterByIsNotChanged()
 {
      return $this->filterBy<?php echo $columnName; ?>(null, CRITERIA::ISNULL);
 }
	
/**
  * Finds objects that have been marked changed
  *
  * <code>
  * $obj  = $c->findIsChanged($con);
  * </code>
  *
  * @param ConnectionInterface $con an optional connection object
  *
  * @return Child<?php echo $table; ?>|array|mixed the result, formatted by the current formatter
  */	
public function findIsChanged( $con = null)
 {
        return $this->filterByIsChanged()->find($con);
 }
	
/**
  * Finds objects that have NOT been marked changed
  *
  * <code>
  * $obj  = $c->findIsNotChanged($con);
  * </code>
  *
  * @param ConnectionInterface $con an optional connection object
  *
  * @return Child<?php echo $table; ?>|array|mixed the result, formatted by the current formatter
  */		
public function findIsNotChanged( $con  = null)
 {
      return $this->filterByIsNotChanged()->find($con);
 }