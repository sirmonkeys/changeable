ChangeableBehavior for [Propel2](https://github.com/propelorm/Propel2)
==================================

The Behavior lets you track changes objects, Similar to the archive bit  in DOS (https://en.wikipedia.org/wiki/Archive_bit)

Installation
------------
Currently the package is not in the composer listing, so you need to add the repo location as well as the package to your `composer.json`. 
*I am hoping to have this fixed soon.*


```json
{
    "minimum-stability": "dev",
    "repositories":[  
            {  
              "type":"vcs",
              "url": "https://bitbucket.org/DarkAxi0m/changeable.git"
            }    
       ],   
    "require": {
        "chrischase/changeable":"*"
    }
}
```

Usage
-----

```xml
    <table name="invoice">
        <column name="id" required="true" primaryKey="true" autoIncrement="true" type="INTEGER" />
        <column name="amount" type="float" />

        <behavior name="changeable"/>
    </table>

    <table name="customer">
        <column name="id" required="true" primaryKey="true" autoIncrement="true" type="INTEGER" />
        <column name="name" type="VARCHAR" />

        <behavior name="changeable">
            <parameter name="changed_column" value="edited"/>
        </behavior>
    </table>


    <table name="product">
        <column name="id" required="true" primaryKey="true" autoIncrement="true" type="INTEGER" />
        <column name="name" type="VARCHAR" />
        <column name="haschanged" type="TIMESTAMP" />

        <behavior name="changeable">
            <parameter name="changed_column" value="haschanged"/>
        </behavior>
    </table>
```

License
-------

See the [LICENSE](LICENSE) file.  (TBA)